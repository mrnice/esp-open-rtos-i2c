# ESP-IDF esp-open-rtos/extras/i2c compatibility component

This ESP-32 ESP-IDF component implements most of the Level 1 API of the
esp-open-rtos/extras/i2c "Yet another I2C driver for the ESP8266" component.

It is intended to be used for easier porting of esp-open-rtos i2c components.

See https://gitlab.com/mrnice/esp-idf-ms561101ba03-example for an example usage.