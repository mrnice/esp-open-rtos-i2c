/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2017 Bernhard Guillon <Bernhard.Guillon@begu.org>
 * Copyright (c) 2015 Johan Kanflo (github.com/kanflo)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <driver/i2c.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <stdio.h>
#include "i2c.h"

//#define I2C_DEBUG true

#define ACK_VAL     0x0
#define NACK_VAL    0x1

#ifdef I2C_DEBUG
#define debug(fmt, ...) printf("%s: " fmt "\n", "I2C", ## __VA_ARGS__)
#else
#define debug(fmt, ...)
#endif


inline bool i2c_status(void)
{
    printf("Unsupported Level 0 stub function 'i2c_status' called. Feel free to implement\n");
    return false;
}

void i2c_init(uint8_t scl_pin, uint8_t sda_pin)
{
    // For now just use this fixed config
    // and allow to set up the scl and sda pins
    i2c_config_t conf;
    conf.mode = I2C_MODE_MASTER;
    conf.sda_io_num = sda_pin;
    conf.scl_io_num = scl_pin;
    conf.sda_pullup_en = GPIO_PULLUP_ENABLE;
    conf.scl_pullup_en = GPIO_PULLUP_ENABLE;
    conf.master.clk_speed = 100000;
    i2c_param_config(I2C_NUM_0, &conf);
    // Install the driver with the given setup
    i2c_driver_install(I2C_NUM_0, I2C_MODE_MASTER, 0, 0, 0);
}

// Output start condition
void i2c_start(void)
{
    printf("Unsupported Level 0 stub function 'i2c_start' called. Feel free to implement\n");
}

// Output stop condition
bool i2c_stop(void)
{
    printf("Unsupported Level 0 stub function 'i2c_stop' called. Feel free to implement\n");
    return false;
}

bool i2c_write(uint8_t byte)
{
    printf("Unsupported Level 0 stub function 'i2c_write' called. Feel free to implement\n");
    return false;
}

uint8_t i2c_read(bool ack)
{
    printf("Unsupported Level 0 stub function 'i2c_read' called. Feel free to implement\n");
    return 0;
}

void i2c_force_bus(bool state)
{
    printf("Unsupported Level 1 stub function 'i2c_force_bus' called. Feel free to implement\n");
}

int i2c_slave_write(uint8_t slave_addr, const uint8_t *data, const uint8_t *buf, uint32_t len)
{
    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);

    i2c_master_write_byte(cmd, (slave_addr << 1) | I2C_MASTER_WRITE, 1 /* expect ack */);

    if(data != NULL)
    {
        i2c_master_write_byte(cmd, *data, 1);
    }

    while(len--)
    {
        i2c_master_write_byte(cmd, *buf++, 1);
    }

    i2c_master_stop(cmd);

    esp_err_t ret = i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000/portTICK_PERIOD_MS);
    i2c_cmd_link_delete(cmd);

    return ret;
}

int i2c_slave_read(uint8_t slave_addr, const uint8_t *data, uint8_t *buf, uint32_t len)
{
    esp_err_t ret = ESP_OK;

    if(data != NULL)
    {
        ret = i2c_slave_write(slave_addr, NULL, data, 1);
    }

    if (ret != ESP_OK)
    {
        return ret;
    }

    if (len == 0)
    {
        return ESP_OK;
    }

    i2c_cmd_handle_t cmd = i2c_cmd_link_create();
    i2c_master_start(cmd);
    i2c_master_write_byte(cmd, (slave_addr << 1) | I2C_MASTER_READ, 1);

    if (len > 1)
    {
        i2c_master_read(cmd, buf, len - 1, ACK_VAL);
    }

    i2c_master_read_byte(cmd, buf + len - 1, NACK_VAL);
    i2c_master_stop(cmd);
    ret = i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_RATE_MS);
    i2c_cmd_link_delete(cmd);

    return ret;
}
